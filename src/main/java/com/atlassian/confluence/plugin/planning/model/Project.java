package com.atlassian.confluence.plugin.planning.model;

import java.util.Collections;
import java.util.List;

public class Project
{
    private final String title;
    private final List<Iteration> iterations;

    public Project(String title, List<Iteration> iterations)
    {
        this.title = title;
        this.iterations = Collections.unmodifiableList(iterations);
    }

    public String getTitle()
    {
        return title;
    }

    public List<Iteration> getIterations()
    {
        return iterations;
    }
}
