package com.atlassian.confluence.plugin.planning;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.json.JSONAction;
import com.atlassian.confluence.json.parser.JSONException;
import com.opensymphony.webwork.ServletActionContext;

import javax.servlet.http.HttpServletResponse;

public class SaveProjectAction extends ConfluenceActionSupport implements JSONAction
{
    private static final String OUT_OF_DATE = "outofdate";

    private String projectName;
    private String data;
    private int version;

    private ProjectManager projectManager;
    private StoredProject project;

    public String execute()
    {
        try
        {
            projectManager.updateProject(projectName, version, data);
            return SUCCESS;
        }
        catch (DataOutOfDateException e)
        {
            ServletActionContext.getResponse().setStatus(HttpServletResponse.SC_CONFLICT);
            project = projectManager.loadProject(projectName);
            return OUT_OF_DATE;
        }
    }

    public String getJSONString() throws JSONException
    {
        return project.getJSONString();
    }

    public String getProjectName()
    {
        return projectName;
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.projectManager = new ProjectManager(bandanaManager);
    }
}
