package com.atlassian.confluence.plugin.planning;

import java.io.Serializable;

public class StoredProject implements Serializable
{
    // XStream-injected - can't be final 
    private String name;
    private int version;
    private String data;

    public StoredProject(String name, int version, String data) {
        this.name = name;
        this.version = version;
        this.data = data;
    }

    public StoredProject(String name, String data) {
        this(name, 0, data);
    }

    public String getName() {
        return name;
    }

    public int getVersion() {
        return version;
    }

    public String getData() {
        return data;
    }

    public String getJSONString() {
        return "{ \"name\": \"" + name.replace("\"", "\\\"") + "\",\n" +
            "\"version\": " + version + ",\n" +
            "\"data\": " + data + "}\n";
    }

    /**
     * Returns a copy of this project with updated data and an incremented version number.
     *
     * @throws DataOutOfDateException if the provided version does not match the current data version
     */
    public StoredProject updateData(int version, String data) throws DataOutOfDateException {
        if (version != this.version)
            throw new DataOutOfDateException();
        return new StoredProject(name, this.version + 1, data);
    }
}
