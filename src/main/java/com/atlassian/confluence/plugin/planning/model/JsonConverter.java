package com.atlassian.confluence.plugin.planning.model;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonConverter
{
    public Map<String, Object> toBean(Project project)
    {
        Map<String, Object> jsonProject = new HashMap<String, Object>(2);
        jsonProject.put("title", StringUtils.defaultString(project.getTitle()));
        List<Map<String, Object>> jsonIterations = new ArrayList<Map<String, Object>>();
        for (Iteration iteration : project.getIterations())
        {
            jsonIterations.add(toBean(iteration));
        }
        jsonProject.put("iterations", jsonIterations);
        return jsonProject;
    }

    private Map<String, Object> toBean(Iteration iteration)
    {
        Map<String, Object> jsonIteration = new HashMap<String, Object>(2);
        jsonIteration.put("title", StringUtils.defaultString(iteration.getTitle()));
        List<Task> tasks = iteration.getTasks();
        List<Map<String, String>> jsonTasks = new ArrayList<Map<String, String>>();
        for (Task task : tasks)
        {
            jsonTasks.add(toBean(task));
        }
        jsonIteration.put("tasks", jsonTasks);
        return jsonIteration;
    }

    private Map<String, String> toBean(Task task)
    {
        Map<String, String> jsonTask = new HashMap<String, String>();
        jsonTask.put("title", StringUtils.defaultString(task.getTitle()));
        jsonTask.put("complete", String.valueOf(task.isCompleted()));
        if (StringUtils.isNotBlank(task.getEstimate())) jsonTask.put("estimate", task.getEstimate());
        if (StringUtils.isNotBlank(task.getNotes())) jsonTask.put("notes", task.getNotes());
        return jsonTask;
    }
}
