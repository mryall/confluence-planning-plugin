package com.atlassian.confluence.plugin.planning;

/**
 * Thrown when an optimistic concurrency check fails.
 */
public class DataOutOfDateException extends Exception {
    public DataOutOfDateException() {
    }

    public DataOutOfDateException(String message) {
        super(message);
    }

    public DataOutOfDateException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataOutOfDateException(Throwable cause) {
        super(cause);
    }
}
