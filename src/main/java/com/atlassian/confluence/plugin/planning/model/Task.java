package com.atlassian.confluence.plugin.planning.model;

public class Task
{
    private final String title;
    private final String estimate;
    private final String notes;
    private final boolean completed;

    public Task(String title, String estimate, String notes, boolean completed)
    {
        this.title = title;
        this.estimate = estimate;
        this.notes = notes;
        this.completed = completed;
    }

    public String getTitle()
    {
        return title;
    }

    public String getEstimate()
    {
        return estimate;
    }

    public String getNotes()
    {
        return notes;
    }

    public boolean isCompleted()
    {
        return completed;
    }
}
