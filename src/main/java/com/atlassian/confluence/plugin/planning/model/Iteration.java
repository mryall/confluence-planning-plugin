package com.atlassian.confluence.plugin.planning.model;

import java.util.*;

public class Iteration
{
    private final String title;
    private final List<Task> tasks;

    public Iteration(String title, List<Task> tasks)
    {
        this.title = title;
        this.tasks = Collections.unmodifiableList(tasks);
    }

    public String getTitle()
    {
        return title;
    }

    public List<Task> getTasks()
    {
        return tasks;
    }
}
