package com.atlassian.confluence.plugin.planning;

import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

public final class ProjectPlanMacro extends BaseMacro
{
    private VelocityHelperService velocityHelperService;

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        if (!parameters.containsKey("name"))
            throw new MacroException("Project plan macro requires a 'name' parameter.");

        Map<String, Object> context = velocityHelperService.createDefaultVelocityContext();
        context.put("projectName", parameters.get("name"));
        return velocityHelperService.getRenderedTemplate("/planning/project-plan-macro.vm", context);
    }

    public void setVelocityHelperService(VelocityHelperService velocityHelperService)
    {
        this.velocityHelperService = velocityHelperService;
    }
}
