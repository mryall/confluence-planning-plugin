package com.atlassian.confluence.plugin.planning;

import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.thoughtworks.xstream.XStream;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ProjectManager
{
    private static final String STORAGE_KEY_PREFIX = "com.atlassian.confluence.plugin.confluence-planning-plugin";
    private static final BandanaContext STORAGE_CONTEXT = ConfluenceBandanaContext.GLOBAL_CONTEXT;

    private final BandanaManager bandanaManager;
    private final XStream xStream;

    public ProjectManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
        xStream = new XStream();
        xStream.setClassLoader(getClass().getClassLoader()); // use the plugin classloader
    }

    public StoredProject loadProject(String projectName)
    {
        String xml = (String) bandanaManager.getValue(STORAGE_CONTEXT, toStorageKey(projectName));
        if (xml == null)
            return new StoredProject(projectName, defaultData());
        return (StoredProject) xStream.fromXML(xml);
    }

    public StoredProject updateProject(String projectName, int version, String data) throws DataOutOfDateException
    {
        StoredProject existingProject = loadProject(projectName);
        StoredProject updatedProject = existingProject.updateData(version, data);
        String xml = xStream.toXML(updatedProject);
        bandanaManager.setValue(STORAGE_CONTEXT, toStorageKey(projectName), xml);
        return updatedProject;
    }

    private static String toStorageKey(String projectName)
    {
        return STORAGE_KEY_PREFIX + "_" + projectName.replaceAll("[^\\p{Alnum}]", "");
    }

    private static String defaultData()
    {
        return "{ \"iterations\": [\n" +
            "   { \"title\": \"Iteration 1: " + getDefaultIterationDate() + "\", \"tasks\": [\n" +
            "       { \"title\": \"Example scheduled task\", \"estimate\": \"1d\" }\n" +
            "   ]},\n" +
            "   { \"title\": \"Backlog\", \"tasks\": [\n" +
            "       { \"title\": \"Example backlog task\", \"estimate\": \"1d 4h\" }\n" +
            "   ]}\n" +
            "]}\n";
    }

    private static String getDefaultIterationDate()
    {
        return new SimpleDateFormat("d MMM").format(new Date());
    }



}
