package com.atlassian.confluence.plugin.planning;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.json.JSONAction;

public class ViewProjectAction extends ConfluenceActionSupport implements JSONAction
{
    private String projectName;
    private StoredProject project;
    private ProjectManager projectManager;

    public String execute()
    {
        project = loadStoredProject(projectName);
        return SUCCESS;
    }

    private StoredProject loadStoredProject(String projectName)
    {
        return projectManager.loadProject(projectName);
    }

    public String getJSONString()
    {
        return project.getJSONString();
    }

    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.projectManager = new ProjectManager(bandanaManager);
    }
}
