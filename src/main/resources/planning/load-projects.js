jQuery(function ($) {
    var contextPath = jQuery("#confluence-context-path").attr("content");
    $(".project-planning-macro").each(function () {
        var element = this;
        var project = new Project({
            element: element,
            dataUrl: contextPath + "/plugins/planning/project.action",
            saveUrl: contextPath + "/plugins/planning/saveproject.action"
        });
    });
});