createProject({
    title: "Engine Room 3.1",
    iterations: [
        {
            title: "Week 1: 1 Jul",
            tasks: [
                { title: "TPM Websphere Build", estimate: "2d", complete: true }
            ]
        },
        {
            title: "Week 2: 8 Jul",
            tasks: [
                { title: "CONF-16287 Fix broken search", estimate: "2h", complete: true },
                { title: "Deploy Renderer & Extras Sources", estimate: "1d", complete: true, notes: "Left over devspeed work for Charles" },
                { title: "Bootstrapping of caches", estimate: "2d", complete: true },
                { title: "Update hibernate caching", estimate: "1d", complete: true },
                { title: "interface cleansing - remove coherence references", estimate: "1d", complete: true },
                { title: "update cache regions", estimate: "1d", complete: true }
            ]
        },
        {
            title: "Week 3: 14 Jul",
            tasks: [
                { title: "Upgrade SAL to 2.0.10 (CONF-16351)", estimate: "1h", complete: true },
                { title: "Move coherence code to separate module", estimate: "1d", complete: true },
                { title: "Get tests passing", estimate: "4h", complete: true },
                { title: "Update setup with license checks", estimate: "4h", complete: true },
                { title: "Update cluster setup to check for coherence", estimate: "4h", complete: true },
                { title: "Update 'update license' to check cluster status", estimate: "4h", complete: true }
            ]
        },
        {
            title: "Week 4: 21 Jul",
            tasks: [
                { title: "Fix PDF plugin", estimate: "1d", complete: true },
                { title: "Fix acceptance tests", estimate: "2h", complete: true },
                { title: "Create 2 bamboo builds", estimate: "2h", complete: true },
                { title: "investigate removing coherence jars from maven repository", estimate: "1d", complete: true },
                { title: "performance testing", estimate: "1d", complete: true },
                { title: "Merge to 3.0 stable & update builds", estimate: "1d", complete: true },
                { title: "Backport to 2.10, 2.9, 2.8, 2.7, 2.6", estimate: "3d", complete: true }
            ]
        },
        {
            title: "Week 5: 28 Jul",
            tasks: [
                { title: "Review/Fix Distributions", estimate: "1d", complete: true },
                { title: "Merge to Trunk & update builds", estimate: "1d", complete: true },
                { title: "Finalise public documents", estimate: "2h", complete: true },
                { title: "Document cache config changes", estimate: "2h", complete: true },
                { title: "Clean up deprecated code", estimate: "1d", complete: true },
                { title: "Hide log window AAM-46", estimate: "1d", complete: true },
                { title: "Replace log messages with progress bars AAM-49", estimate: "4h", complete: true },
                { title: "Apply UI design for AAM", estimate: "1d", complete: true },
                { title: "Plugin code reviews (see Don for list)", estimate: "1d", complete: true }
            ]
        },
        {
            title: "Week 6: 4 Aug",
            tasks: [
                { title: "Update log4j to suppress ehcache warnings", estimate: "1h", complete: true },
                { title: "Update setup wizard for clustered (CONF-16492)", estimate: "2h", complete: true },
                { title: "AAM-56 Prompt for server port on first install", estimate: "1d", complete: true },
                { title: "Update atlassian plugins to support retrieval of resources by type i.e. js/css", estimate: "1d", complete: true },
                { title: "Update atlassian plugins and move JS to bottom", estimate: "1d", complete: true }
            ]
        },
        {
            title: "Week 7: 11 Aug",
            tasks: [
                { title: "Fix inline permissions JS so we can move JS to bottom", estimate: "2d", complete: true },
                { title: "Spike REST API for Confluence", estimate: "3d", complete: true },
                { title: "{color:#0000ff}Fedex 12{color}", complete: true }
            ]
        },
        {
            title: "Week 8: 18 Aug",
            tasks: [
                { title: "Planning for REST API", estimate: "1h", complete: true },
                { title: "Fix FF3 build", estimate: "4h", complete: true }
            ]
        },
        {
            title: "Week 9: 25 Aug",
            tasks: [
                { title: "Finish extracting test code", estimate: "4h", complete: true },
                { title: "REST - Tests for Spaces, Content", estimate: "2d", complete: true },
                { title: "Prepare for REST presentation", estimate: "4h", complete: true },
                { title: "REST - Investigate efficient paging", estimate: "2d", complete: true }
            ]
        },
        {
            title: "Week 10: 1 Sep",
            tasks: [
                { title: "Remove tangosol.jsp", estimate: "1h", complete: true },
                { title: "Fix REST test code to use test properties", estimate: "1h", complete: true }
            ]
        },
        {
            title: "Week 11: 8 Sep",
            tasks: [
                { title: "Fix REST Build", estimate: "2h", complete: true },
                { title: "REST - Finish off getSpaces()", estimate: "1d", complete: true },
                { title: "REST - Finish getSpace() with permissions", estimate: "1d", complete: true }
            ]
        },
        {
            title: "Week 12: 15 Sep",
            tasks: [
                { title: "CONF-16886 Fix performance builds (upgrade plugins)", estimate: "1d", complete: true },
                { title: "CONF-16772 XML export with attachments broken", estimate: "1d", complete: true }
            ]
        },
        {
            title: "Week 13: 22 Sep",
            tasks: [
                { title: "CONF-16686 Implement disable plugins temporarily", estimate: "1d", complete: true },
                { title: "REST - Page Children expansion", estimate: "2d", complete: true }
            ]
        },
        {
            title: "Week 14: 29 Sep",
            tasks: [
                { title: "CONF-17045 Javascript bottom backwards compatibility mode", estimate: "1d", complete: true },
                { title: "Publish deprecated methods doc and forum post", estimate: "1h", complete: true }
            ]
        },
        {
            title: "Week 15: 6 Oct",
            tasks: [
                { title: "REST - Refactor Spaces list", estimate: "4h", complete: true },
                { title: "REST - Update REST module with better list support", estimate: "1d", complete: true }
            ]
        },
        {
            title: "Week 16: 13 Oct",
            tasks: [
                { title: "CONF-17014 Plugins and Weblogic", estimate: "1d", complete: true },
                { title: "CONF-17046 Page Comments Expansion", estimate: "2d", complete: true },
                { title: "CONF-17075 Non existent plugin action stops Confluence from starting", estimate: "1d", complete: true }
            ]
        },
        {
            title: "Week 17: 20 Oct",
            tasks: [
                { title: "CONF-17045 Make javascript top default and remove warning", estimate: "4h", complete: true }
            ]
        },
        {
            title: "Week 18: 27 Oct",
            tasks: [
                { title: "CONF-17151 Super batch confluence web resources", estimate: "2d", complete: true },
                { title: "Rest Documentation", estimate: "2h", complete: true },
                { title: "Fix Copy space plugin for 3.1", complete: true }
            ]
        },
        {
            title: "Week 19: 3 Nov",
            tasks: [
                { title: "Builds - TPM Websphere", complete: true },
                { title: "Builds - TPM Tomcat6 + Oracle 10", complete: true },
                { title: "CONF-17469 Gallery icons not showing due to superbatch", estimate: "1d", complete: true }
            ]
        },
        {
            title: "Week 20: 10 November",
            tasks: [
                { title: "Performance Regression Investigation" },
                { title: "Gliffy errors on EAC" },
                { title: "CONF-17417 TaskQueueJob no longer uses getQueueName()" },
                { title: "CONF-17040 Create profile for building from source without coherence", estimate: "4h" }
            ]
        },
        {
            title: "Backlog",
            tasks: [
                { title: "CONF-16722 Viewing old version of page on EAC" },
                { title: "Release plugins 2.4.0" },
                { title: "CONF-13833 Sprite images - automate SmartSprite", estimate: "1d" },
                { title: "Add SmartSprite declaratives to CSS files", estimate: "1d" },
                { title: "Spike/Investigate cluster panic on one node", estimate: "2d", notes: "Why do we get so many cluster panics? Try to reproduce reliably" },
                { title: "JSON call for agressive caching scripts", estimate: "4h" },
                { title: "Apply Dmitry's agressive caching script", notes: "Use ajax not iframes" },
                { title: "Eliminate/Reduce XML parsing libraries", estimate: "2d" },
                { title: "CONF-11764 Remove concurrent util backport & use Java 5 API", estimate: "1d" },
                { title: "Spike JS minification in build process", estimate: "2d", notes: "see how JIRA does this" },
                { title: "Automate REST Documentation" },
                { title: "Automate cache configs", estimate: "2d", notes: "We currently have two cache config files (ehcache and coherence specific ones)." }
            ]
        }
    ]
});
