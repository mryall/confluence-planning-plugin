createProject({
    title: "Insert Link Dialog for 3.2",
    iterations: [
        {
            title: "Iteration 1: 29 Dec",
            tasks: [
                { title: "Dialog widget sidebar", estimate: "2d", complete: true },
                { title: "Dialog widget body", estimate: "2d", complete: true }
            ]
        },
        {
            title: "Iteration 2: 5 Jan",
            tasks: [
                { title: "Selenium tests and builds", estimate: "1d", complete: true },
                { title: "Quick nav panel", estimate: "2d", complete: true, notes: "Took two days longer than expected." }
            ]
        },
        {
            title: "Iteration 3: 12 Jan",
            tasks: [
                { title: "Search panel", estimate: "3d" },
                { title: "Update search JSON to return link info - REST", estimate: "1d", notes: "Pair with ER team" },
                { title: "Web link panel & edit existing link", estimate: "1d" }
            ]
        },
        {
            title: "Backlog",
            tasks: [
                { title: "Link dialog CSS", estimate: "2d" },
                { title: "Profile to get 'pi' working", estimate: "2h" },
                { title: "Web Link panel + edit existing link", estimate: "4h" },
                { title: "Edit existing link - populate quick nav panel", estimate: "4h" },
                { title: "Selected text used as alias", estimate: "4h" }
            ]
        }
    ]
});
