// global variable
var Project;

(function () {
    var $ = jQuery;
    if (!window.console) window.console = { log: function () {} };
    
    // Shared functions
    function renderTemplate(name) {
        return $($("script[title=" + name + "]:first").text());
    }
    function sum(array) {
        var sum = 0;
        for (var i=0; i<array.length; i++)
            sum += array[i];
        return sum;
    }
    function formatText(text) {
//        text = text.replace(/&/, "&amp;").replace(/</, "&lt;").replace(/>/, "&gt;");
//        text = text.replace(/\b(CONF-\d+)\b/, "<a href='http://jira.atlassian.com/browse/$1'>$1</a>");
        if (text.match(/^\s*$/)) text = "&nbsp;"; // ensure HTML isn't empty
        return text;
    }
    function parseHours(text) {
        var components = text.split(/\s+/);
        var hours = 0;
        for (var i=0; i<components.length; i++) {
            var component = components[i].match(/^\s*(\d+)([hd])\s*$/) || ["", 0, "h"];
            hours += component[1] * (component[2] == "d" ? 8 : 1);
        }
        return hours;
    }
    function formatHours(hours) {
        var result = [];
        if (hours >= 8) result.push(parseInt(hours / 8, 10) + "d");
        if (hours % 8 > 0) result.push(hours % 8 + "h");
        return result.join(" ");
    }
    function ignoreClick(e) {
        e.stopPropagation();
        e.preventDefault();
        return false;
    }

    // Internal objects
    var Task = function (project, title, estimate, notes) {
        var task = this;
        task.$el = renderTemplate("task-template");
        task.$el[0].task = task; // backlink from the element to this object
        task.complete = function () {
            task.$el.addClass("complete");
            task.$el.find(".editable").addClass("disabled");
            project.saveChanges();
            return task;
        };
        task.uncomplete = function () {
            task.$el.removeClass("complete");
            task.$el.find(".editable").removeClass("disabled");
            project.saveChanges();
            return task;
        };
        task.getData = function () {
            return {
                title: this.$el.find(".title").html(),
                estimate: this.$el.find(".estimate").text(),
                notes: this.$el.find(".notes").html(),
                complete: this.$el.hasClass("complete")
            };
        };
        $(".status", task.$el).click(function (e) {
            if ($("body").hasClass("editable-disabled")) return;
            
            if (task.$el.hasClass("complete")) task.uncomplete();
            else task.complete();
            return ignoreClick(e);
        });
        if (title) this.$el.find(".title").html(formatText(title));
        if (estimate) this.$el.find(".estimate").text(estimate);
        if (notes) this.$el.find(".notes").html(formatText(notes));
    };
    var Iteration = function (project, title) {
        this.$el = renderTemplate("iteration-template");
        this.$el[0].iteration = this;
        this.addTask = function (title, estimate, notes) {
            var task = new Task(project, title, estimate, notes);
            var target = this.$el;
            while (target.next().hasClass("task"))
                target = target.next();
            task.$el.insertAfter(target);
            if (typeof title == "undefined") {
                task.$el.find(".title").click();
            } else {
                project.saveChanges();
            }
            return task;
        };
        this.calculateHours = function () {
            var task = this.$el.next();
            var result = {
                total: 0,
                completed: 0
            };
            while (task.hasClass("task")) {
                var hours = parseHours(task.find(".estimate").text());
                if (task.hasClass("complete")) result.completed += hours;
                result.total += hours;
                task = task.next();
            }
            return result;
        };
        this.recalculate = function () {
            var hours = this.calculateHours();
            this.$el.find(".estimate").text(formatHours(hours.total));
            this.$el.toggleClass("complete", hours.completed == hours.total);
        };
        var obj = this;
        this.$el.find(".add-task").click(function () {
            var task = obj.addTask();
            return false;
        });
        this.getTitle = function () {
            return this.$el.find(".title").text();
        };
        if (title) this.$el.find(".title").html(formatText(title));
        this.getData = function () {
            var data = { title: this.getTitle(), tasks: [] };
            var task = this.$el.next();
            while (task.hasClass("task")) {
                data.tasks.push(task[0].task.getData());
                task = task.next();
            }
            return data;
        };
    };
    
    // public objects
    Project = function (options) {
        var el = options.element;
        var dataUrl = options.dataUrl;
        var saveUrl = options.saveUrl;

        el.project = this; // backlink to this object from the element
        var $el = $(el);
        var $items = $el.find(".items");
        var backlog = new Iteration(this, "Backlog");
        backlog.$el.addClass("backlog").find(".title").removeClass("editable");
        $items.append(backlog.$el);
        this.$el = $el;
        this.version = 0; // overridden when data is loaded
        var loadingData = false;
        this.getBacklog = function () {
            return backlog;
        };
        this.getTitle = function () {
            return $el.find("h3.title").text();
        };
        this.addIteration = function (title) {
            var iteration = new Iteration(this, title);
            iteration.$el.insertBefore(backlog.$el);
            if (typeof title == "undefined") {
                iteration.$el.find(".title").click();
            } else {
                this.saveChanges();
            }
            return iteration;
        };
        this.recalculate = function () {
            // remove tasks and iterations with no title
            $el.find(".items > *").each(function () {
                if ($(this).find(".title").text().match(/^\s*$/)) {
                    $(this).remove();
                }
            });
            // recalculate iteration totals
            $el.find(".items > .iteration").each(function () {
                this.iteration.recalculate();
            });
            if ($el.find(".charts").is(":visible")) {
                this.refreshCharts();
            }
        };
        
        function getChartData() {
            var data = {
                labels: [],
                hours: {
                    completed: [],
                    remaining: [],
                    total: []
                },
                colors: {
                    completed: '68df61',
                    remaining: 'df6861'
                }
            };
            $el.find(".items > .iteration").each(function () {
                data.labels.push(this.iteration.getTitle().replace(/\s*:.*$/, '').replace(/^(Week|Iteration) /, ''));
                var hours = this.iteration.calculateHours();
                data.hours.total.push(hours.total);
                data.hours.completed.push(hours.completed);
                data.hours.remaining.push(hours.total - hours.completed);
            });
            data.summary = {
                total: sum(data.hours.total),
                completed: sum(data.hours.completed),
                remaining: sum(data.hours.remaining)
            };
            data.totalHoursRoundedToNextDay = Math.ceil(data.totalHours / 8) * 8;
            return data;
        }
        
        this.drawCharts = function () {
            var data = getChartData();
            $(".project .project-chart").gchart({
                title: 'Project summary',
                type: 'barHoriz',
                maxValue: data.summary.total,
                margins: [10, 0, 0, 0], /* left, right, top, bottom */
                legend: 'top',
                series: [
                    $.gchart.series('Completed', [ data.summary.completed ], data.colors.completed),
                    $.gchart.series('Remaining', [ data.summary.remaining ], data.colors.remaining)
                ],
                axes: [
                    $.gchart.axis('bottom', 0, 100, 20),
                    $.gchart.axis('bottom', ['Percentage of total estimate (%)'], [50])
                ]
            });
            var nearestDay = Math.ceil(Math.max.apply(Math, data.hours.total) / 8);
            $(".project .iteration-chart").gchart({
                title: 'Iteration velocity',
                type: 'barVert',
                maxValue: nearestDay * 8,
                barWidth: $.gchart.barWidthAuto,
                dataLabels: data.labels,
                series: [
                    $.gchart.series('Completed', data.hours.completed, data.colors.completed),
                    $.gchart.series('Remaining', data.hours.remaining, data.colors.remaining)
                ],
                axes: [
                    $.gchart.axis('bottom', data.labels, 'black'),
                    $.gchart.axis('left', 0, nearestDay)
                ]
            });
        };
        this.refreshCharts = function () {
            var data = getChartData();
            $(".project .project-chart").gchart('change', {
                maxValue: data.summary.total,
                series: [
                    $.gchart.series('Completed', [ data.summary.completed ], data.colors.completed),
                    $.gchart.series('Remaining', [ data.summary.remaining ], data.colors.remaining)
                ]
            });
            var nearestDay = Math.ceil(Math.max.apply(Math, data.hours.total) / 8);
            $(".project .iteration-chart").gchart('change', {
                maxValue: nearestDay * 8,
                dataLabels: data.labels,
                series: [
                    $.gchart.series('Completed', data.hours.completed, data.colors.completed),
                    $.gchart.series('Remaining', data.hours.remaining, data.colors.remaining)
                ],
                axes: [
                    $.gchart.axis('bottom', data.labels, 'black'),
                    $.gchart.axis('left', 0, nearestDay)
                ]
            });
        };
        
        this.hideCompleted = function () {
            $el.find(".iteration").each(function () {
                var iteration = $(this);
                if (!iteration.hasClass("complete")) return false; // break
                var task = iteration.next();
                iteration.addClass("hidden");
                while (task.hasClass("task")) {
                    var nextTask = task.next(); // handle the fact that the node moves
                    task.addClass("hidden");
                    task = nextTask;
                }
            });
            $el.find(".items .hidden").fadeOut("slow");
        };
        this.showCompleted = function () {
            $el.find(".items .hidden").removeClass("hidden").fadeIn("slow");
        };

        var obj = this;
        $el.find(".header .add-iteration").click(function () {
            obj.addIteration();
            return false;
        });
        obj.drawCharts();
        $el.find(".header .show-chart").toggle(function () {
            $el.find(".charts").slideDown();
            obj.refreshCharts();
            $(this).text("Hide charts");
        }, function () {
            $el.find(".charts").slideUp();
            $(this).text("Show charts");
        });

        $el.find(".header .hide-completed").toggle(function () {
            obj.hideCompleted();
            $(this).text("Show completed");
        }, function () {
            obj.showCompleted();
            $(this).text("Hide completed");
        });
        $el.find(".items").sortable({
            handle: '.handle',
            placeholder: 'drag-placeholder',
            update: function () {
                obj.saveChanges();
            }
        });

        this.setData = function (newProject) {
            loadingData = true; // prevent recalculation while loading data
            var project = this;
            project.version = newProject.version;
            $el.find(".items > :not(.backlog)").remove(); // remove existing elements
            project.iterations = []; // remove existing data
            var iteration, currentIteration, task, currentTask;
            for (var i=0; i<newProject.data.iterations.length; i++) {
                iteration = newProject.data.iterations[i];
                currentIteration = (iteration.title == "Backlog") ?
                    project.getBacklog() :
                    project.addIteration(iteration.title);
                for (var t=0; t<iteration.tasks.length; t++) {
                    task = iteration.tasks[t];
                    currentTask = currentIteration.addTask(task.title, task.estimate, task.notes);
                    if (task.complete) currentTask.complete();
                }
            }
            loadingData = false;
            this.recalculate();
        };

        this.getData = function () {
            var data = {
                iterations: []
            };
            $el.find(".iteration").each(function () {
                data.iterations.push(this.iteration.getData());
            });
            return data;
        };

        this.saveChanges = function () {
            if (loadingData) return;
            (function (version) {
                $.ajax({
                    url: saveUrl,
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        projectName: obj.getTitle(),
                        version: version,
                        data: JSON.stringify(obj.getData())
                    },
                    error: function (xhr, message) {
                        if (xhr.status == 409)
                        {
                            alert("This project has already been updated by another user.\n\n" +
                                  "Your last change will be reverted and the project updated to the latest version.");
                            var data = xhr.responseText;
                            data = jQuery.parseJSON ? jQuery.parseJSON(data) : window["eval"]("(" + data + ")");
                            obj.setData(data);
                        }
                        else
                        {
                            alert("Unknown error updating project data: " + message);
                            console.log(xhr);
                        }
                    },
                    success: function (data) {
                        console.log("Successfully updated data to version: " + data.version);
                    }
                });
            })(obj.version);
            obj.version++; // assume our changes are going to succeed and set the version up for the next change
            this.recalculate();
        };

        // load initial data
        $.ajax({
            url: dataUrl,
            dataType: 'json',
            data: {
                projectName: obj.getTitle()
            },
            success: function (data) {
                $el.removeClass("loading");
                obj.setData(data);
            }
        });
    };
    
    // DOM-ready initialisation
    jQuery(function ($) {
        $(".editable").live("click", function (e) {
            var $el = $(this);
            var $body = $("body");
        
            function restore(e, newValue) {
                $el.empty().html(formatText(newValue));
                $(document).unbind("click.editable");
                $body.removeClass("editable-disabled").addClass("editable-enabled");
                $el.closest(".project")[0].project.saveChanges();
                return ignoreClick(e);
            }
        
            if (!$(e.target).hasClass("editable") || $el.hasClass("disabled")) return;
            if ($body.hasClass("editable-disabled")) return;
            $body.removeClass("editable-enabled").addClass("editable-disabled");
            var originalValue = $el.text();
            var width = $el.width();
            var $input = $("<input type='text'>").click(ignoreClick).width(width + "px");
            $input.val(originalValue);
            $el.empty().append($input);
            $input.keydown(function (e) {
                if (e.which == 9) {
                    restore(e, $input.val());

                    // tab to next editable field
                    var editables = $(".editable:not(.disabled)");
                    var len = editables.length;
                    for (var i=0; i<len; i++) {
                        if (editables[i] == $el[0]) break;
                    }
                    if (i == len) return false;
                
                    var next = e.shiftKey ? i - 1 : i + 1;
                    next = (next + len) % len; // make sure index is within range
                    if (next != null) $(editables[next]).click();
                    return false;
                }
                if (e.which == 13) return restore(e, $input.val());
                if (e.which == 27) return restore(e, originalValue);
            }).focus().select();
            $(document).bind("click.editable", function (e) {
                return restore(e, $input.val());
            });
        
            return ignoreClick(e);
        });
        $("body").addClass("editable-enabled");
    });
})();
